class Student < ActiveRecord::Base
  has_many :student_attachments , dependent: :destroy
  accepts_nested_attributes_for :student_attachments, :allow_destroy => true

  validates_presence_of :first_name
  validates_presence_of :last_name
  validates :year, presence: true, format: {with: /\d{4}/}
  validates :priority, numericality:{less_than_or_equal: 4}

  default_scope -> {order(priority: :asc)}
end
