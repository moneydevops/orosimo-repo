class StudentAttachment < ActiveRecord::Base
  belongs_to :student
  mount_uploader :avatar, AvatarUploader
end
