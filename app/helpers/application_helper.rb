module ApplicationHelper

  #dialegei mia tuxaia eikona apo to fakelo me tis fwtografies gia to home page i opou allou to baloume
  def random_image
    image_files = %w( .jpg .JPG .png )
      files = Dir.entries(
            "#{Rails.root.join('app/assets/images/epitixontes/')}"
             ).delete_if { |x| !image_files.index(x[-4,4]) }
       @rnd_pic = files[rand(files.length)]
       @picTitle =  @rnd_pic.slice(0...(@rnd_pic.index('.')))
       @picTitle1 =  @rnd_pic.slice(0...(@rnd_pic.index('.')))
       @studName = @picTitle.split(' - ')[0]
       @studUni = @picTitle.split(' - ')[1]
      return @rnd_pic
  end

  def fetch_images
    image_files = %w( .jpg .JPG .png )
    files = Dir.entries(
          "#{Rails.root.join('app/assets/images/epitixontes-special/')}"
          ).sort.drop(2)

    files += Dir.entries(
          "#{Rails.root.join('app/assets/images/epitixontes/')}"
          ).sort.drop(2)

  end


  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "Ορόσημο Πειραιά"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title 
    end
  end
end
