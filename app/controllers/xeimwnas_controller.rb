class XeimwnasController < ApplicationController
  before_action :set_xeimwna, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_model!, except: [:index]

  # GET /xeimwnas
  # GET /xeimwnas.json
  def index
    @xeimwnas = Xeimwna.all
  end

  # GET /xeimwnas/1
  # GET /xeimwnas/1.json
  def show
  end

  # GET /xeimwnas/new
  def new
    @xeimwna = Xeimwna.new
  end

  # GET /xeimwnas/1/edit
  def edit
  end

  # POST /xeimwnas
  # POST /xeimwnas.json
  def create
    @xeimwna = Xeimwna.new(xeimwna_params)

    respond_to do |format|
      if @xeimwna.save
        format.html { redirect_to @xeimwna, notice: 'Xeimwna was successfully created.' }
        format.json { render :show, status: :created, location: @xeimwna }
      else
        format.html { render :new }
        format.json { render json: @xeimwna.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /xeimwnas/1
  # PATCH/PUT /xeimwnas/1.json
  def update
    respond_to do |format|
      if @xeimwna.update(xeimwna_params)
        format.html { redirect_to @xeimwna, notice: 'Xeimwna was successfully updated.' }
        format.json { render :show, status: :ok, location: @xeimwna }
      else
        format.html { render :edit }
        format.json { render json: @xeimwna.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /xeimwnas/1
  # DELETE /xeimwnas/1.json
  def destroy
    @xeimwna.destroy
    respond_to do |format|
      format.html { redirect_to xeimwnas_url, notice: 'Xeimwna was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_xeimwna
      @xeimwna = Xeimwna.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def xeimwna_params
      params.require(:xeimwna).permit(:body)
    end
end
