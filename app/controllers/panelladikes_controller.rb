class PanelladikesController < ApplicationController
  before_action :set_panelladike, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_model!, except: [:index, :show]

  # GET /panelladikes
  # GET /panelladikes.json
  def index
    @panelladikes = Panelladike.all
  end

  # GET /panelladikes/1
  # GET /panelladikes/1.json
  def show
    @panelladikes = Panelladike.all
  end

  # GET /panelladikes/new
  def new
    @panelladike = Panelladike.new
  end

  # GET /panelladikes/1/edit
  def edit
  end

  # POST /panelladikes
  # POST /panelladikes.json
  def create
    @panelladike = Panelladike.new(panelladike_params)

    respond_to do |format|
      if @panelladike.save
        format.html { redirect_to @panelladike, notice: 'Panelladike was successfully created.' }
        format.json { render :show, status: :created, location: @panelladike }
      else
        format.html { render :new }
        format.json { render json: @panelladike.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /panelladikes/1
  # PATCH/PUT /panelladikes/1.json
  def update
    respond_to do |format|
      if @panelladike.update(panelladike_params)
        format.html { redirect_to @panelladike, notice: 'Panelladike was successfully updated.' }
        format.json { render :show, status: :ok, location: @panelladike }
      else
        format.html { render :edit }
        format.json { render json: @panelladike.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /panelladikes/1
  # DELETE /panelladikes/1.json
  def destroy
    @panelladike.destroy
    respond_to do |format|
      format.html { redirect_to panelladikes_url, notice: 'Panelladike was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_panelladike
      @panelladike = Panelladike.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def panelladike_params
      params.require(:panelladike).permit(:body, :lesson)
    end
end
