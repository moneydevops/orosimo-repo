class StaticPagesController < ApplicationController
  def home
  end

  def taftotita
  end

  def frontistirio
  end

  def kalokairi
  end

  def programma
  end

  def lukeio
  end


  def xeimwnas
  end

  def panelladikes
  end

  def ximeia
  end

  def fusiki
  end

  def mathimatika
  end

  def glwssa
  end

  def latinika
  end

  def istoria
  end

  def biologia
  end

  def aoth
  end

  def arxaia
  end

  def aep
  end

  def ecdl
  end

  def contact
  end

  def epitixontes
  end

  def dialekse_kateuthinsi
  end

  def analitika_thetiki
  end
end
