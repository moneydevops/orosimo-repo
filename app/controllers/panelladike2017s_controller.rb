class Panelladike2017sController < ApplicationController
  before_action :set_panelladike2017, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_model!, except: [:index, :show]

  # GET /panelladike2017s
  # GET /panelladike2017s.json
  def index
    @panelladike2017s = Panelladike2017.all
  end

  # GET /panelladike2017s/1
  # GET /panelladike2017s/1.json
  def show
    @panelladike2017s = Panelladike2017.all
  end

  # GET /panelladike2017s/new
  def new
    @panelladike2017 = Panelladike2017.new
  end

  # GET /panelladike2017s/1/edit
  def edit
  end

  # POST /panelladike2017s
  # POST /panelladike2017s.json
  def create
    @panelladike2017 = Panelladike2017.new(panelladike2017_params)

    respond_to do |format|
      if @panelladike2017.save
        format.html { redirect_to @panelladike2017, notice: 'Panelladike2017 was successfully created.' }
        format.json { render :show, status: :created, location: @panelladike2017 }
      else
        format.html { render :new }
        format.json { render json: @panelladike2017.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /panelladike2017s/1
  # PATCH/PUT /panelladike2017s/1.json
  def update
    respond_to do |format|
      if @panelladike2017.update(panelladike2017_params)
        format.html { redirect_to @panelladike2017, notice: 'Panelladike2017 was successfully updated.' }
        format.json { render :show, status: :ok, location: @panelladike2017 }
      else
        format.html { render :edit }
        format.json { render json: @panelladike2017.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /panelladike2017s/1
  # DELETE /panelladike2017s/1.json
  def destroy
    @panelladike2017.destroy
    respond_to do |format|
      format.html { redirect_to panelladike2017s_url, notice: 'Panelladike2017 was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_panelladike2017
      @panelladike2017 = Panelladike2017.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def panelladike2017_params
      params.require(:panelladike2017).permit(:body, :lesson)
    end
end
