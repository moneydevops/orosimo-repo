class KalokairisController < ApplicationController
  before_action :set_kalokairi, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_model!, except: [:index]

  # GET /kalokairis
  # GET /kalokairis.json
  def index
    @kalokairis = Kalokairi.all
  end

  # GET /kalokairis/1
  # GET /kalokairis/1.json
  def show
  end

  # GET /kalokairis/new
  def new
    @kalokairi = Kalokairi.new
  end

  # GET /kalokairis/1/edit
  def edit
  end

  # POST /kalokairis
  # POST /kalokairis.json
  def create
    @kalokairi = Kalokairi.new(kalokairi_params)

    respond_to do |format|
      if @kalokairi.save
        format.html { redirect_to @kalokairi, notice: 'Kalokairi was successfully created.' }
        format.json { render :show, status: :created, location: @kalokairi }
      else
        format.html { render :new }
        format.json { render json: @kalokairi.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /kalokairis/1
  # PATCH/PUT /kalokairis/1.json
  def update
    respond_to do |format|
      if @kalokairi.update(kalokairi_params)
        format.html { redirect_to @kalokairi, notice: 'Kalokairi was successfully updated.' }
        format.json { render :show, status: :ok, location: @kalokairi }
      else
        format.html { render :edit }
        format.json { render json: @kalokairi.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /kalokairis/1
  # DELETE /kalokairis/1.json
  def destroy
    @kalokairi.destroy
    respond_to do |format|
      format.html { redirect_to kalokairis_url, notice: 'Kalokairi was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_kalokairi
      @kalokairi = Kalokairi.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def kalokairi_params
      params.require(:kalokairi).permit(:id,:body)
    end
end
