class StudentAttachmentsController < ApplicationController
  before_action :set_student_attachment, only: [:show, :edit, :update, :destroy]

  # GET /student_attachments
  # GET /student_attachments.json
  def index
    @student_attachments = StudentAttachment.all
  end

  # GET /student_attachments/1
  # GET /student_attachments/1.json
  def show
  end

  # GET /student_attachments/new
  def new
    @student_attachment = StudentAttachment.new
  end

  # GET /student_attachments/1/edit
  def edit
  end

  # POST /student_attachments
  # POST /student_attachments.json
  def create
    @student_attachment = StudentAttachment.new(student_attachment_params)

    respond_to do |format|
      if @student_attachment.save
        format.html { redirect_to @student_attachment, notice: 'Student attachment was successfully created.' }
        format.json { render :show, status: :created, location: @student_attachment }
      else
        format.html { render :new }
        format.json { render json: @student_attachment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /student_attachments/1
  # PATCH/PUT /student_attachments/1.json
  def update
    respond_to do |format|
      if @student_attachment.update(student_attachment_params)
        format.html { redirect_to @student_attachment, notice: 'Student attachment was successfully updated.' }
        format.json { render :show, status: :ok, location: @student_attachment }
      else
        format.html { render :edit }
        format.json { render json: @student_attachment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /student_attachments/1
  # DELETE /student_attachments/1.json
  def destroy
    @student_attachment.destroy
    respond_to do |format|
      format.html { redirect_to student_attachments_url, notice: 'Student attachment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student_attachment
      @student_attachment = StudentAttachment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def student_attachment_params
      params.require(:student_attachment).permit(:student_id, :avatar)
    end
end
