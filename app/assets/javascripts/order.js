var ready, set_positions;

set_positions = function(){
  $('.panel-sortable').each(function(i){
    $(this).attr("data-pos",i+1);
  });
}

ready = function(){
  //dinei ta data-pos gia ta stoixeia mesa sto div
  set_positions();

  $('.sortable').sortable({
    
    axis: "y",
    handle: ".my-handle",
    placeholder: 'sortable-placeholder',
    revert: 250,

    start: function(e,ui){

      placeholderHeight = ui.item.outerHeight();
      ui.placeholder.height(placeholderHeight + 15);
      $('<div class="sortable-placeholder-animator" data-height="'+placeholderHeight+'"></div>').insertAfter(ui.placeholder);
    },

    change: function(event, ui) {
        
      ui.placeholder.stop().height(0).animate({
        height: ui.item.outerHeight() + 15
      }, 300);
        
      placeholderAnimatorHeight = parseInt($(".sortable-placeholder-animator").attr("data-height"));
        
      $(".sortable-placeholder-animator").stop().height(placeholderAnimatorHeight + 15).animate({
        height: 0
      }, 300, function() {
      $(this).remove();
      placeholderHeight = ui.item.outerHeight();
      $('<div class="sortable-placeholder-animator" data-height="' + placeholderHeight + '"></div>').insertAfter(ui.placeholder);
        });
        
    },

    update: function(e,ui){

    //array to store new order
    updated_order = []

    set_positions();

    $('.panel-sortable').each(function(i){
      updated_order.push({ id: $(this).data("id"),position: i+1 });
    });

    //send data to controller
    $.ajax({
      type: "PUT",
      url: '/students/order',
      data: { order: updated_order}
    });
    },

    stop: function(e, ui) {
              
     $(".sortable-placeholder-animator").remove();
                      
    }
});
}

$(document).ready(ready);
