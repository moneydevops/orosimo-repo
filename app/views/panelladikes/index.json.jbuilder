json.array!(@panelladikes) do |panelladike|
  json.extract! panelladike, :id, :body, :lesson
  json.url panelladike_url(panelladike, format: :json)
end
