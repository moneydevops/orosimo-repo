json.array!(@latests) do |latest|
  json.extract! latest, :id, :body
  json.url latest_url(latest, format: :json)
end
