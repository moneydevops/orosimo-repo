json.array!(@student_attachments) do |student_attachment|
  json.extract! student_attachment, :id, :student_id, :avatar
  json.url student_attachment_url(student_attachment, format: :json)
end
