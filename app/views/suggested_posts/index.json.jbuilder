json.array!(@suggested_posts) do |suggested_post|
  json.extract! suggested_post, :id, :body, :lesson
  json.url suggested_post_url(suggested_post, format: :json)
end
