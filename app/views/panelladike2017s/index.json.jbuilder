json.array!(@panelladike2017s) do |panelladike2017|
  json.extract! panelladike2017, :id, :body, :lesson
  json.url panelladike2017_url(panelladike2017, format: :json)
end
