json.array!(@kalokairis) do |kalokairi|
  json.extract! kalokairi, :id
  json.url kalokairi_url(kalokairi, format: :json)
end
