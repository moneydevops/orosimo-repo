class CreatePanelladikes < ActiveRecord::Migration
  def change
    create_table :panelladikes do |t|
      t.string :body
      t.string :lesson

      t.timestamps null: false
    end
  end
end
