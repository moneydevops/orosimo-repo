class CreateStudentAttachments < ActiveRecord::Migration
  def change
    create_table :student_attachments do |t|
      t.integer :student_id
      t.string :avatar

      t.timestamps null: false
    end
  end
end
