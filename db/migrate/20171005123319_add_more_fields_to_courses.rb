class AddMoreFieldsToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :body, :string
    add_column :courses, :title, :string
  end
end
