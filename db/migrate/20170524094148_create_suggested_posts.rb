class CreateSuggestedPosts < ActiveRecord::Migration
  def change
    create_table :suggested_posts do |t|
      t.string :body
      t.string :lesson

      t.timestamps null: false
    end
  end
end
