class CreateLatests < ActiveRecord::Migration
  def change
    create_table :latests do |t|
      t.string :body

      t.timestamps null: false
    end
  end
end
