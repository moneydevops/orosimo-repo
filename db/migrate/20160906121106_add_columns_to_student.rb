class AddColumnsToStudent < ActiveRecord::Migration
  def change
    add_column :students, :last_name, :string
    add_column :students, :school, :string
    add_column :students, :year, :integer
    add_column :students, :priority, :integer
  end
end
