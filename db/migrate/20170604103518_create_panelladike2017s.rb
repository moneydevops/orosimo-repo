class CreatePanelladike2017s < ActiveRecord::Migration
  def change
    create_table :panelladike2017s do |t|
      t.string :body
      t.string :lesson

      t.timestamps null: false
    end
  end
end
