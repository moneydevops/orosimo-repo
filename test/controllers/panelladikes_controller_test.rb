require 'test_helper'

class PanelladikesControllerTest < ActionController::TestCase
  setup do
    @panelladike = panelladikes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:panelladikes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create panelladike" do
    assert_difference('Panelladike.count') do
      post :create, panelladike: { body: @panelladike.body, lesson: @panelladike.lesson }
    end

    assert_redirected_to panelladike_path(assigns(:panelladike))
  end

  test "should show panelladike" do
    get :show, id: @panelladike
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @panelladike
    assert_response :success
  end

  test "should update panelladike" do
    patch :update, id: @panelladike, panelladike: { body: @panelladike.body, lesson: @panelladike.lesson }
    assert_redirected_to panelladike_path(assigns(:panelladike))
  end

  test "should destroy panelladike" do
    assert_difference('Panelladike.count', -1) do
      delete :destroy, id: @panelladike
    end

    assert_redirected_to panelladikes_path
  end
end
