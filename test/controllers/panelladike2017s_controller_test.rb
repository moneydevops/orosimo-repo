require 'test_helper'

class Panelladike2017sControllerTest < ActionController::TestCase
  setup do
    @panelladike2017 = panelladike2017s(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:panelladike2017s)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create panelladike2017" do
    assert_difference('Panelladike2017.count') do
      post :create, panelladike2017: { body: @panelladike2017.body, lesson: @panelladike2017.lesson }
    end

    assert_redirected_to panelladike2017_path(assigns(:panelladike2017))
  end

  test "should show panelladike2017" do
    get :show, id: @panelladike2017
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @panelladike2017
    assert_response :success
  end

  test "should update panelladike2017" do
    patch :update, id: @panelladike2017, panelladike2017: { body: @panelladike2017.body, lesson: @panelladike2017.lesson }
    assert_redirected_to panelladike2017_path(assigns(:panelladike2017))
  end

  test "should destroy panelladike2017" do
    assert_difference('Panelladike2017.count', -1) do
      delete :destroy, id: @panelladike2017
    end

    assert_redirected_to panelladike2017s_path
  end
end
