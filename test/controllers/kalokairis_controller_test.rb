require 'test_helper'

class KalokairisControllerTest < ActionController::TestCase
  setup do
    @kalokairi = kalokairis(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:kalokairis)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create kalokairi" do
    assert_difference('Kalokairi.count') do
      post :create, kalokairi: {  }
    end

    assert_redirected_to kalokairi_path(assigns(:kalokairi))
  end

  test "should show kalokairi" do
    get :show, id: @kalokairi
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @kalokairi
    assert_response :success
  end

  test "should update kalokairi" do
    patch :update, id: @kalokairi, kalokairi: {  }
    assert_redirected_to kalokairi_path(assigns(:kalokairi))
  end

  test "should destroy kalokairi" do
    assert_difference('Kalokairi.count', -1) do
      delete :destroy, id: @kalokairi
    end

    assert_redirected_to kalokairis_path
  end
end
