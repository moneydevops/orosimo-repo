require 'test_helper'

class StudentAttachmentsControllerTest < ActionController::TestCase
  setup do
    @student_attachment = student_attachments(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:student_attachments)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create student_attachment" do
    assert_difference('StudentAttachment.count') do
      post :create, student_attachment: { avatar: @student_attachment.avatar, student_id: @student_attachment.student_id }
    end

    assert_redirected_to student_attachment_path(assigns(:student_attachment))
  end

  test "should show student_attachment" do
    get :show, id: @student_attachment
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @student_attachment
    assert_response :success
  end

  test "should update student_attachment" do
    patch :update, id: @student_attachment, student_attachment: { avatar: @student_attachment.avatar, student_id: @student_attachment.student_id }
    assert_redirected_to student_attachment_path(assigns(:student_attachment))
  end

  test "should destroy student_attachment" do
    assert_difference('StudentAttachment.count', -1) do
      delete :destroy, id: @student_attachment
    end

    assert_redirected_to student_attachments_path
  end
end
