require 'test_helper'

class XeimwnasControllerTest < ActionController::TestCase
  setup do
    @xeimwna = xeimwnas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:xeimwnas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create xeimwna" do
    assert_difference('Xeimwna.count') do
      post :create, xeimwna: { body: @xeimwna.body }
    end

    assert_redirected_to xeimwna_path(assigns(:xeimwna))
  end

  test "should show xeimwna" do
    get :show, id: @xeimwna
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @xeimwna
    assert_response :success
  end

  test "should update xeimwna" do
    patch :update, id: @xeimwna, xeimwna: { body: @xeimwna.body }
    assert_redirected_to xeimwna_path(assigns(:xeimwna))
  end

  test "should destroy xeimwna" do
    assert_difference('Xeimwna.count', -1) do
      delete :destroy, id: @xeimwna
    end

    assert_redirected_to xeimwnas_path
  end
end
