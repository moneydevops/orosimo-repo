Rails.application.routes.draw do
  resources :courses
  resources :panelladike2017s
  resources :suggested_posts
  resources :panelladikes
  resources :xeimwnas
  resources :kalokairis
  resources :latests
  mount Ckeditor::Engine => '/ckeditor'
  root 'static_pages#home'	
  devise_for :models, controllers: {sessions: 'models/sessions'}

  resources :students do
    put :order, on: :collection
  end

  get "home"=> "static_pages#home"
  
  get "lista" => "students#list"
  get "taftotita" => "static_pages#taftotita"
  get "frontistirio" => "static_pages#frontistirio"
  get "ecdl" => "static_pages#ecdl"
  get "contact" => "static_pages#contact"
  get "epitixontes"=>"students#index"
  get "dialekse_kateuthinsi"=>"static_pages#dialekse_kateuthinsi"
  get "analitika_thetiki"=>"static_pages#analitika_thetiki"

  get "programma" => "static_pages#programma"
  get "programma_lukeiou" => 'static_pages#lukeio'

  get "themata_ximeia"=>'static_pages#ximeia'
  get "themata_fusiki"=>'static_pages#fusiki'
  get "themata_mathimatika"=>'static_pages#mathimatika'
  get "themata_glwssa"=>'static_pages#glwssa'
  get "themata_latinika"=>'static_pages#latinika'
  get "themata_istoria"=>'static_pages#istoria'
  get "themata_biologia"=>'static_pages#biologia'
  get "themata_aoth"=>'static_pages#aoth'
  get "themata_arxaia"=>'static_pages#arxaia'
  get "themata_aep"=>'static_pages#aep'
  

  get "anthropistikes"=>'static_pages#anthropistikes'
  get "thetikes"=>'static_pages#thetikwn'
  get "oikonomias_pliroforikis"=>'static_pages#oikonomias'
 
 
  get "prosfora" => 'static_pages#prosfora'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
